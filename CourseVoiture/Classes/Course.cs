﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseVoiture
{
    class Course
    {
        List<Voiture> Voitures;
        Circuit Circuit;
        int NombreTours;

        public void Start()
        {
            InitEnvironment();
            ConfigureTrack();
            ConfigureCars();
            Run();
            DisplayWinner();
        }

        public void InitEnvironment()
        {
        
            Voitures = new List<Voiture>();
            Circuit = new Circuit();
            NombreTours = 0;
        }

        public void ConfigureTrack()
        {
            Console.Write("Sur quel circuit la course se déroule : ");
            Circuit.Nom = Console.ReadLine();

            Console.Write("Quelle est la distance au tour du circuit (en KM) : ");
            Circuit.Distance = double.Parse(Console.ReadLine());

            Console.Write("Combien de tours faut-il faire : ");
            NombreTours= int.Parse(Console.ReadLine());

            Utils.Continue();
        }

        public void ConfigureCars()
        {
            Console.Write("Combien de Voitures sont en Course : ");
            int nbCars = int.Parse(Console.ReadLine());

            for(int i = 1; i<= nbCars; i++)
            {
                Console.WriteLine("-------- Voiture {0} -------", i);
                Voiture v = new Voiture();
                v.Tours = new List<Tour>();
                Console.Write("Marque de la voiture : ");
                v.Marque = Console.ReadLine();
                Console.Write("Modèle de la voiture : ");
                v.Modele = Console.ReadLine();
                Console.Write("Vitesse minimale de la voiture : ");
                v.VMin= int.Parse(Console.ReadLine());
                Console.Write("Vitesse maximale de la voiture : ");
                v.VMax = int.Parse(Console.ReadLine());

                Voitures.Add(v);
            }

            Utils.Continue();
        }

        public void Run()
        {
            for(int i = 1; i<= NombreTours; i++)
            {
                foreach (Voiture v in Voitures)
                {
                    Tour t = new Tour();
                    t.VTour = v.getAverageSpeed();
                    t.getDuree(Circuit.Distance);
                    v.Tours.Add(t);
                }
            }

            Console.WriteLine("La course est finie!!");

            Utils.Continue();
        }

        public void DisplayWinner()
        {
            Voiture winner =
                 Voitures
                 .Where(
                     x => x.Tours.Sum(y => y.Duree.TotalSeconds)
                     ==
                     Voitures.Min(y => y.Tours.Sum(z => z.Duree.TotalSeconds))
                  ).FirstOrDefault();

            Voiture looser = Voitures.Where(x => x.Tours.Sum(y => y.Duree.TotalSeconds) == Voitures.Max(y => y.Tours.Sum(z => z.Duree.TotalSeconds))).FirstOrDefault();

            Console.WriteLine("\nEt le Gagnant est la {0} {1}\n", winner.Marque, winner.Modele);
            for(int i = 1; i<= winner.Tours.Count(); i++)
            {
                Console.WriteLine("Tour {0} fini en {1} à une vitesse moyenne de {2} km/h", i, winner.Tours[i - 1].Duree, winner.Tours[i - 1].VTour);
            }

            Console.WriteLine("\nLe Perdant est la {0} {1}\n", looser.Marque, looser.Modele);
            for (int i = 1; i <= looser.Tours.Count(); i++)
            {
                Console.WriteLine("Tour {0} fini en {1} à une vitesse moyenne de {2} km/h", i, looser.Tours[i - 1].Duree, looser.Tours[i - 1].VTour);
            }
        }


    }
}
