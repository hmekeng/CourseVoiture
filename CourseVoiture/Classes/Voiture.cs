﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseVoiture
{
    class Voiture
    {
        public string Marque { get; set; }
        public string Modele { get; set; }
        public int VMin { get; set; }
        public int VMax { get; set; }
        public List<Tour> Tours {get;set;}
        public Random rnd = new Random();

        public int getAverageSpeed()
        {
            return rnd.Next(VMin, VMax + 1);
        }
    }
}
