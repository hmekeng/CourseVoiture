﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseVoiture
{
    class Tour
    {
        public TimeSpan Duree { get; set; }
        public double VTour { get; set; }

        public void getDuree(double distance)
        {
            double HoursCalcul = Math.Round(distance / VTour, 2);
            double MinutesCalcul, SecondsCalcul;
            int Hours = (int)Math.Floor(distance / VTour);
            MinutesCalcul = ((HoursCalcul - Hours) * 60) / ((Hours != 0) ? Hours : 1);
            int Minutes = (int)Math.Floor(MinutesCalcul);
            SecondsCalcul = ((MinutesCalcul - Minutes) * 60) / ((Minutes != 0) ? Minutes : 1);
            int Seconds = (int)Math.Floor(SecondsCalcul);

            Duree = new TimeSpan(Hours, Minutes, Seconds);
        }
    }
}
